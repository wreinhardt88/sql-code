if object_id('insert_statements') is not null
	begin
		drop procedure dbo.insert_statements
	end
go

 create procedure dbo.insert_statements
 @all int = 0,
 @tables varchar(max) = null,
 @debug int = 0
 as
 begin
	declare @begin_insert as nvarchar(max) = null,
			@insert_columns as nvarchar(max) = null,
			@end_insert as nvarchar(max) = null,
			@statement as nvarchar(max) = null,
			@column_list varchar(50),				-- For use with column cursor
			@table_list varchar(255)
	
	if @all = 1
	begin
		declare tab_c cursor for
		select TABLE_NAME from information_schema.tables where table_type = 'BASE TABLE'

		open tab_c

		fetch next from tab_c
		into @table_list

		while @@fetch_status = 0
		begin
			-- Set the beginning of the insert statement
			set @begin_insert = (select 'insert into dbo.' + tablename + '(' from mig_column_list where tablename = @table_list)

			-- Sets list of columns used for insert/statements
			declare col_c cursor for
			select column_name from mig_column_list where tablename = @table_list and Include_column = 'Y'

			open col_c

			fetch next from col_c
			into @column_list

			while @@fetch_status = 0
			begin
				-- create list of values with commas to seperate the columns
				if @insert_columns is null and @@FETCH_STATUS = 0
				begin
					set @insert_columns = '[' + @column_list + ']'
				end
				else
					set @insert_columns = @insert_columns + ',[' + @column_list + ']'

				fetch next from col_c into @column_list
				--print @column_list
				--print @insert_columns
			end

			close col_c
			DEALLOCATE col_c

			-- Completes the beginning of the insert statement
			set @begin_insert = @begin_insert + @insert_columns + ')'

			set @end_insert = ' select ' + @insert_columns + ' from ' + @table_list
			set @statement = @begin_insert + @end_insert

			-- Rest variables
			set @begin_insert = null
			set @insert_columns = null
			set @end_insert = null
			set @column_list = null

			print @statement
			fetch next from tab_c into @table_list
		end

		close tab_c
		DEALLOCATE tab_c
	end

	if @tables is not null and @debug = 2
	begin
		print '@tables has values'
	end
	if @tables is not null
	begin
		-- Selects the list of tables from a pre-created table loaded prior to the execution of this procedure
		declare tab_c cursor for
		select tablename from mig_table_list order by tablename

		open tab_c

		fetch next from tab_c
		into @table_list
		
		if @debug = 2
		begin
			print '@table_list'
			print @table_list
		end

		while @@fetch_status = 0
		begin
			-- Set the beginning of the insert statement
			set @begin_insert = (select 'insert into dbo.' + @table_list + '(')
				
				-- IMAGE COLUMNS
				if @table_list = 'wf_model'
				BEGIN
					set @begin_insert = (select 'insert into dbo.wf_model_temp(')
				END
								
				if @table_list = 'wf_queue'
				BEGIN
					set @begin_insert = (select 'insert into dbo.wf_queue_temp(')
				END
				
			-- Sets list of columns used for insert/statements
			declare col_c cursor for
			select column_name from mig_column_list where tablename = @table_list and include_column = 'Y'

			open col_c

			fetch next from col_c
			into @column_list

			while @@fetch_status = 0
			begin
				-- create list of values with commas to seperate the columns
				if @insert_columns is null and @@FETCH_STATUS = 0
				begin
					set @insert_columns = '[' + @column_list + ']'
				end
				else
					set @insert_columns = @insert_columns + ',[' + @column_list + ']'

				fetch next from col_c into @column_list
				--print @column_list
				--print @insert_columns
			end

			close col_c
			DEALLOCATE col_c

			-- Completes the beginning of the insert statement
			set @begin_insert = @begin_insert + @insert_columns + ')'
									
			--set @end_insert = ' select ' + @insert_columns + ' from ' + @table_list
			
			set @end_insert = ' select ' + @insert_columns + ' from devl77.devl77.bsidba.' + @table_list
			
			set @statement = @begin_insert + @end_insert

			if @debug=1
			begin
				print '@begin_insert'
				print @begin_insert
				print ''
				print '@insert_columns'
				print @insert_columns
				print ''
				print '@end_insert'
				print @end_insert
				print ''
				print '@statement'
				print @statement
				print ''
			end
			
			-- Rest variables
			set @begin_insert = null
			set @insert_columns = null
			set @end_insert = null
			set @column_list = null
			
		
			/****************************/
			/**** EXECUTE STATEMENT *****/
			/****************************/
			declare @create_table nvarchar(max)
			
			if @table_list = 'wf_model'
			begin
				
				set @create_table = 'CREATE TABLE [dbo].[wf_model_temp](
										[wf_begin_date] [datetime] NULL,
										[wf_description] [varchar](255) NULL,
										[wf_event_id] [char](8) NULL,
										[wf_model_id] [char](16) NOT NULL,
										[wf_object] [char](36) NULL,
										[wf_reentrancy] [char](2) NULL,
										[wf_rules] nvarchar(max) NULL,
										[wf_status] [char](2) NULL,
										[wf_version] [int] NOT NULL,
										[unique_key] [char](36) NOT NULL
										) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]'
										
				if object_id('wf_model_temp') is not null
				begin
					drop table dbo.wf_model_temp
				end
				EXECUTE sp_executesql @create_table		
			end
			
			if @table_list = 'wf_queue'
			begin
			
				set @create_table = 'CREATE TABLE [dbo].[wf_queue_temp](
									[wf_creator] [char](12) NULL,
									[wf_event_id] [char](8) NULL,
									[wf_key] [char](36) NULL,
									[wf_object] [char](36) NULL,
									[wf_processing_eng] [char](36) NULL,
									[wf_record_data] [nvarchar](max) NULL,
									[wf_start] [datetime] NULL,
									[unique_key] [char](36) NOT NULL
									) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]'
				if object_id('wf_queue_temp') is not null
				begin
					drop table dbo.wf_queue_temp
				end
				EXECUTE sp_executesql @create_table
			end
			
			if @debug = 0
			begin
				EXECUTE sp_executesql @statement
			end
			/****************************/
			/****************************/
			/****************************/
			
			
			-- IMAGE DATA
			if @table_list = 'wf_model'
			begin
				Set @statement = 'insert into wf_model
									select wf_begin_date,wf_description,wf_event_id,wf_model_id,wf_object,
											wf_reentrancy,cast(cast(wf_rules as varbinary(max)) as image),
											wf_status,wf_version,unique_key
									from wf_model_temp'
				if @debug=0
				begin
					EXECUTE sp_executesql @statement
				end
			end
			
			if @table_list = 'wf_queue'
			begin
				Set @statement = 'insert into wf_queue
									select [wf_creator],[wf_event_id],[wf_key],[wf_object],[wf_processing_eng],
									cast(cast(wf_record_data as varbinary(max)) as image),[wf_start],[unique_key]
									from wf_queue_temp'
				if @debug=0
				begin
					EXECUTE sp_executesql @statement
				end
			end
			
			if @debug=1
			begin
				print '@statement'
				print @statement
				print ''
			end
			
			if @debug = 2
			begin
				print '@table_list'
				print @table_list
			end

			fetch next from tab_c into @table_list
		end

		close tab_c
		DEALLOCATE tab_c
	end

end