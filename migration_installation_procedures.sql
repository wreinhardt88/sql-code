------------------------------------------------------------
-- Stored Procedure to clean up database				  --
-- Created By: William Reinhardt						  --
-- Created On: 1/8/14									  --
------------------------------------------------------------

-- Check to see if sp_par already exists, if it does it will be dropped.
IF OBJECT_ID('sp_cleanup') IS NOT NULL
	BEGIN
		DROP PROCEDURE dbo.sp_cleanup
	END
GO

CREATE PROCEDURE dbo.sp_cleanup
AS
BEGIN
	DECLARE @sqlstring	NVARCHAR(MAX) -- Used for executing statements
	
	-- Creating a cursor to SELECT the ALTER and DROP statements into a variable
	DECLARE c_cur CURSOR FOR
		(
			-- This SELECT statement will conjoin 3 different SELECT statements to
			-- collect all unnecessary triggers, indexes and constraints
			-- from ar_cust_mstr,,ar_custtype_mstr and ar_type_mstr
			SELECT 'DROP TRIGGER ' + name FROM sys.triggers 
			WHERE parent_id IN(OBJECT_ID('ar_cust_mstr'),OBJECT_ID('ar_custtype_mstr'),OBJECT_ID('ar_type_mstr')) 
			AND type = 'TR' 
			AND (name LIKE '%ar_acct%' OR name LIKE '%ar_acterm%' OR name LIKE '%ar_term%')
				UNION
			SELECT distinct 'DROP INDEX ' + I.[name] + ' ON ' + T.[name]
			FROM sys.[tables] AS T  
			INNER JOIN sys.[indexes] I ON T.[object_id] = I.[object_id]  
			INNER JOIN sys.[index_columns] IC ON I.[object_id] = IC.[object_id] 
			INNER JOIN sys.[all_columns] AC ON T.[object_id] = AC.[object_id] AND IC.[column_id] = AC.[column_id] 
			WHERE T.[is_ms_shipped] = 0 AND I.[type_desc] <> 'HEAP' 
			AND t.name in ('ar_type_mstr','ar_custtype_mstr','ar_cust_mstr')
			AND (i.name LIKE '%ar_term_%' OR i.name LIKE '%ar_acct%' OR i.name LIKE '%ar_acterm%')
				UNION
			SELECT 'ALTER TABLE ' + t.name + ' DROP ' + o.name FROM sysobjects o 
			INNER JOIN syscolumns c on o.id = c.cdefault 
			INNER JOIN sysobjects t on c.id = t.id 
			where o.xtype in ('D') 
			AND t.name in ('ar_type_mstr','ar_cust_mstr','ar_custtype_mstr')
			AND (o.name LIKE '%ar_acct%' OR o.name LIKE '%ar_acterm%' OR o.name LIKE '%ar_term%')
		)
	
	-- Opens cursor created above and selects the first result into a varable.
	OPEN c_cur
	FETCH NEXT FROM c_cur INTO @sqlstring
	
	-- The statements executed will be shown in the results.
	-- If no statements were ran, no results will follow.
	PRINT 'Statements executed: '
	PRINT '--------------------'
	PRINT '' -- Line break
	
	-- If there are results in the cursor then start loop
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC sp_executesql @sqlstring	-- Executes ALTER or DROP statement.
		PRINT @sqlstring				-- Prints statement for results.
		
		FETCH NEXT FROM c_cur INTO @sqlstring	-- Selects the next record to execute, if none, then exit.
	END
	
	CLOSE c_cur			-- Close the cursor
	DEALLOCATE c_cur	-- Deallocates cursor memory stored
	
END
GO

------------------------
-- End sp_cleanup.sql --
------------------------


------------------------------------------------------------
-- Stored Procedure to fix SQL Server issues			  --
-- Created By: William Reinhardt						  --
-- Created On: 1/9/14									  --
------------------------------------------------------------

-- Check to see if sp_par already exists, if it does it will be dropped.
IF OBJECT_ID('sp_sqlfixes') IS NOT NULL
	BEGIN
		DROP PROCEDURE dbo.sp_sqlfixes
	END
GO

CREATE PROCEDURE dbo.sp_sqlfixes
AS
BEGIN
	DECLARE @sqlstring	NVARCHAR(MAX),	-- Used for executing ALTER statements that use a variable
			@sqlstringdeux	NVARCHAR(MAX)	-- Used for executing ALTER statements that use a variable
	
	-- 	Prevent "Affected row(s)" in output	
	SET NOCOUNT ON
	
	PRINT ''
	PRINT '-- Statements to add default constraints'
	PRINT '----------------------------------------'
	
	SET @sqlstring = NULL			-- Reset executable sql string to NULL
	DECLARE addcon_cur CURSOR FOR	-- Creating cursor for adding constraints.
	(
		-- Create a script to add default value of "newid()" to unique_key columns
		-- with no default value.
		SELECT 'ALTER TABLE ' + tab.name + ' ADD CONSTRAINT DF_' + tab.name + '_' + col.name + ' DEFAULT newid() FOR ' + col.name
		FROM syscolumns col, sysobjects tab
		WHERE col.name='unique_key'
		  AND RIGHT(tab.name, 2) != '_l'
		  AND tab.xtype = 'U'
		  AND tab.id = col.id
		  AND col.cdefault = 0
	)
	
	-- Opens cursor addcon_cur and selects the first result into a varable if there is one.
	OPEN addcon_cur
	FETCH NEXT FROM addcon_cur INTO @sqlstring
	
		WHILE @@FETCH_STATUS = 0
			BEGIN
				EXEC sp_executesql @sqlstring	-- Executes statement to add default constraint.
				PRINT @sqlstring				-- Prints statement executed.
				
				FETCH NEXT FROM addcon_cur INTO @sqlstring	-- Selects the next record to execute, if none, then exit.
			END -- end while loop
	
		CLOSE addcon_cur	-- Close the cursor
	DEALLOCATE addcon_cur	-- Deallocates cursor memory stored
	
	PRINT ''
	PRINT '-- Statements to update columns'
	PRINT '-------------------------------'
	
	SET @sqlstring = NULL			-- Reset executable sql string to NULL
	DECLARE upcol_cur CURSOR FOR	-- Creating cursor for updating columns.
	(
		-- Generate a script to populate all NULL unique_key columns with newid()
		SELECT 'UPDATE ' + tab.name + ' SET ' + col.name + ' = newid() WHERE unique_key is null'
		FROM syscolumns col, sysobjects tab
		WHERE col.name='unique_key'
		  AND RIGHT(tab.name, 2) != '_l'
		  AND tab.xtype = 'U'
		  AND tab.id = col.id
	)
	
	-- Opens cursor upcol_cur and selects the first result into a varable if there is one.
	OPEN upcol_cur
	FETCH NEXT FROM upcol_cur INTO @sqlstring
	
		WHILE @@FETCH_STATUS = 0
			BEGIN
				EXEC sp_executesql @sqlstring 	-- Executes ALTER or DROP statement.
				PRINT @sqlstring				-- Prints statement for results.
				
				FETCH NEXT FROM upcol_cur INTO @sqlstring	-- Selects the next record to execute, if none, then exit.
			END -- end while loop
	
		CLOSE upcol_cur		-- Close the cursor
	DEALLOCATE upcol_cur	-- Deallocates cursor memory stored
	
	PRINT ''
	PRINT '-- Statements to change the datatype of columns'
	PRINT '-----------------------------------------------'
	
	SET @sqlstring = NULL			-- Reset executable sql string to NULL
	DECLARE altcol_cur CURSOR FOR	-- Creating cursor for changing the datatype of columns.
	(
		-- Generate a script to change all unique_key columns to NOT NULL
		SELECT 'ALTER TABLE ' + tab.name + ' ALTER COLUMN ' + col.name + ' char(36) NOT NULL'
		FROM syscolumns col, sysobjects tab
		WHERE col.name='unique_key'
		  AND RIGHT(tab.name, 2) != '_l'
		  AND tab.xtype = 'U'
		  AND tab.id = col.id
		  AND col.isnullable = 1
	)
	
	-- Opens cursor altcol_cur and selects the first result into a varable if there is one.
	OPEN altcol_cur
	FETCH NEXT FROM altcol_cur INTO @sqlstring
	
		WHILE @@FETCH_STATUS = 0
			BEGIN
				EXEC sp_executesql @sqlstring	-- Executes ALTER or DROP statement.
				PRINT @sqlstring				-- Prints statement for results.
				
				FETCH NEXT FROM altcol_cur INTO @sqlstring	-- Selects the next record to execute, if none, then exit.
			END -- end while loop
	
		CLOSE altcol_cur	-- Close the cursor
	DEALLOCATE altcol_cur	-- Deallocates cursor memory stored

	PRINT ''
	PRINT '-- Statements to re-create unique_id columns'
	PRINT '--------------------------------------------'

	SET @sqlstring = NULL			-- Reset executable sql string to NULL
	DECLARE drpcol_cur CURSOR FOR	-- Creating cursor for dropping UNIQUE_ID columns that are not identity columns
	(
		-- Generate statements to drop unique_id columns
		SELECT 'ALTER TABLE ' + tab.name + ' DROP COLUMN ' + col.name
		FROM syscolumns col, sysobjects tab
		WHERE col.name='unique_id'
		  AND RIGHT(tab.name, 2) != '_l'
		  AND col.status != 128
		  AND tab.xtype = 'U'
		  AND tab.id = col.id
		  AND (tab.name != 'pe_name_mstr_temp' AND tab.name != 'pe_addr_dtl_temp')
	)
	SET @sqlstringdeux = NULL		-- Reset second executable sql string to NULL
	DECLARE addcol_cur CURSOR FOR	-- Creating cursor for adding UNIQUE_ID identity columns
	(
		-- Generate statements to re-create unique_id columns
		-- as IDENTITY for all tables if unique_id is not already 'identity'
		SELECT 'ALTER TABLE ' + tab.name + ' ADD ' + col.name + ' integer identity'
		FROM syscolumns col, sysobjects tab
		WHERE col.name='unique_id'
		  AND RIGHT(tab.name, 2) != '_l'
		  AND col.status != 128
		  AND tab.xtype = 'U'
		  AND tab.id = col.id
		  AND (tab.name != 'pe_name_mstr_temp' AND tab.name != 'pe_addr_dtl_temp')
	)
	
	OPEN drpcol_cur
	OPEN addcol_cur
	FETCH NEXT FROM drpcol_cur INTO @sqlstring
	FETCH NEXT FROM addcol_cur INTO @sqlstringdeux
	
		WHILE @@FETCH_STATUS = 0
			BEGIN
				EXEC sp_executesql @sqlstring		-- Executes DROP COLUMN statement.
				EXEC sp_executesql @sqlstringdeux	-- Executes ALTER ADD COLUMN statement.
				PRINT @sqlstring					-- Prints statement for results.
				PRINT @sqlstringdeux				-- Prints statement for results.
				
				FETCH NEXT FROM drpcol_cur INTO @sqlstring		-- Selects the next record to execute, if none, then exit.
				FETCH NEXT FROM addcol_cur INTO @sqlstringdeux	-- Selects the next record to execute, if none, then exit
			END -- end while loop
	
		CLOSE drpcol_cur	-- Close the cursor
		CLOSE addcol_cur	-- Close the cursor
	DEALLOCATE drpcol_cur	-- Deallocates cursor memory stored
	DEALLOCATE addcol_cur	-- Deallocates cursor memory stored

END
GO

------------------------
-- End sp_sqlfixes.sql --
------------------------