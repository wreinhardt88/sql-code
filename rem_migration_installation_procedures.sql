------------------------------------------------------------
-- Procedure removal script								  --
-- Created By: William Reinhardt						  --
-- Created On: 1/8/14									  --
------------------------------------------------------------

IF OBJECT_ID('sp_par') IS NOT NULL
	BEGIN
		DROP PROCEDURE dbo.sp_par
		PRINT 'dbo.sp_par has been removed.'
	END
GO

IF OBJECT_ID('sp_cleanup') IS NOT NULL
	BEGIN
		DROP PROCEDURE dbo.sp_cleanup
		PRINT 'dbo.sp_cleanup has been removed.'
	END
GO

IF OBJECT_ID('sp_sqlfixes') IS NOT NULL
	BEGIN
		DROP PROCEDURE dbo.sp_sqlfixes
		PRINT 'dbo.sp_sqlfixes has been removed.'
	END

IF OBJECT_ID('sp_nonascii') IS NOT NULL
	BEGIN
		DROP PROCEDURE dbo.sp_nonascii
		PRINT 'dbo.sp_nonascii has been removed.'
	END
GO

------------------------
-- End removal script --
------------------------