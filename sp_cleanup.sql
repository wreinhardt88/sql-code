------------------------------------------------------------
-- StORed Procedure to clean up database				  --
-- Created By: William Reinhardt						  --
-- Modified On: 1/7/14									  --
------------------------------------------------------------

-- Check to see if sp_par already exists, if it does it will be dropped.
IF OBJECT_ID('sp_cleanup') IS NOT NULL
	BEGIN
		DROP PROCEDURE dbo.sp_cleanup
	END
GO

CREATE PROCEDURE dbo.sp_cleanup
AS
BEGIN
	DECLARE @sqlstring	NVARCHAR(MAX) -- Used for executing statements
	DECLARE c_cur CURSOR FOR
		(
			SELECT 'DROP TRIGGER ' + name FROM sys.triggers 
			WHERE parent_id IN(OBJECT_ID('ar_cust_mstr'),OBJECT_ID('ar_custtype_mstr'),OBJECT_ID('ar_type_mstr')) 
			AND type = 'TR' 
			AND (name LIKE '%ar_acct%' OR name LIKE '%ar_acterm%' OR name LIKE '%ar_term%')
			UNION 
			SELECT distinct 'DROP INDEX ' + I.[name] + ' ON ' + T.[name]
			FROM sys.[tables] AS T  
			  INNER JOIN sys.[indexes] I ON T.[object_id] = I.[object_id]  
			  INNER JOIN sys.[index_columns] IC ON I.[object_id] = IC.[object_id] 
			  INNER JOIN sys.[all_columns] AC ON T.[object_id] = AC.[object_id] AND IC.[column_id] = AC.[column_id] 
			WHERE T.[is_ms_shipped] = 0 AND I.[type_desc] <> 'HEAP' 
			AND t.name in ('ar_type_mstr','ar_custtype_mstr','ar_cust_mstr')
			AND (i.name LIKE '%ar_term_%' OR i.name LIKE '%ar_acct%' OR i.name LIKE '%ar_acterm%')
			UNION
			SELECT 'ALTER TABLE ' + t.name + ' DROP ' + o.name FROM sysobjects o 
			INNER JOIN syscolumns c on o.id = c.cdefault 
			INNER JOIN sysobjects t on c.id = t.id 
			where o.xtype in ('D') 
			AND t.name in ('ar_type_mstr','ar_cust_mstr','ar_custtype_mstr')
			AND (o.name LIKE '%ar_acct%' OR o.name LIKE '%ar_acterm%' OR o.name LIKE '%ar_term%')
		)
		
	OPEN c_cur
	FETCH NEXT FROM c_cur INTO @sqlstring
	
	PRINT 'Statements executed: '
	PRINT '--------------------'
	PRINT ''
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC sp_executesql @sqlstring
		PRINT @sqlstring
		
		FETCH NEXT FROM c_cur INTO @sqlstring
	END
	
	CLOSE c_cur
	DEALLOCATE c_cur

END
GO

------------------------
-- End sp_cleanup.sql --
------------------------