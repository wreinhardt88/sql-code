if object_id('sp_columnlist') is not null
	begin
		drop procedure dbo.sp_columnlist
	end
go

if object_id('mig_column_list') is null
begin
	create table dbo.mig_column_list(tablename varchar(255) not null, 
									Column_Name varchar(255) not null, 
									Include_Column char(1) not null default 'Y',
									Check_For_Non_Ascii char(1) not null default 'N',
									Clean_Bad_Dates char(1) not null default 'N',
									primary key (tablename,Column_Name))
end
go

create procedure dbo.sp_columnlist
as
begin
		declare @table varchar(255), @column varchar(255), @column_list nvarchar(max)

		

		declare col_c cursor for
			select table_name,column_name from information_schema.columns where column_name != 'unique_id'

			open col_c

			fetch next from col_c
			into @table,@column

			while @@fetch_status = 0
			begin
				-- Inserts table, column for each column in a table into mig_column_list
				-- the records will be used for column selection in migration tool
				set @column_list = 'insert into dbo.mig_column_list(tablename,Column_Name) values(''' + @table + ''',''' + @column + ''')'
				
				exec sp_executesql @column_list

				fetch next from col_c into @table,@column
			end
		close col_c
		DEALLOCATE col_c
end