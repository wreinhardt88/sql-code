set nocount on

declare @start int
declare @end int
declare @count int
declare @statements nvarchar(max)

SET @start = 1
SET @end = 1000

set @count = (select COUNT(*) from (select ROW_NUMBER() over(order by name) as rn,  'DROP SYNONYM [' + object_schema_name(object_id) + '].[' + name + ']' as statements 
						from sys.synonyms where object_schema_name(object_id) = 'CHICO\Ali.Guzeldere') sy 
						where sy.rn between @start and @end)
						
While @count != 0
	begin
		select statements from (select ROW_NUMBER() over(order by name) as rn,  'DROP SYNONYM [' + object_schema_name(object_id) + '].[' + name + ']' as statements 
						from sys.synonyms where object_schema_name(object_id) = 'CHICO\Ali.Guzeldere') sy 
		where sy.rn between @start and @end
		select 'go'
		SET @start = @start + 1000
		set @end = @end + 1000
		select @count = COUNT(*) from (select ROW_NUMBER() over(order by name) as rn,  'DROP SYNONYM [' + object_schema_name(object_id) + '].[' + name + ']' as statements 
						from sys.synonyms where object_schema_name(object_id) = 'CHICO\Ali.Guzeldere') sy 
						where sy.rn between @start and @end
	end
set nocount off