------------------------------------------------------------
-- Stored Procedure for use with Post Action Report (PAR) --
-- Created By: William Reinhardt						  --
-- Created On: 1/7/14									  --
-- Last Modified 9/26/14								  --
------------------------------------------------------------

-- Check to see if sp_par already exists, if it does it will be dropped.
IF OBJECT_ID('sp_par') IS NOT NULL
	BEGIN
		DROP PROCEDURE dbo.sp_par
	END
GO

CREATE PROCEDURE dbo.sp_par
@base_v		NUMERIC(4,2), -- Base version number of OS
@end_v		NUMERIC(4,2), -- End version number of OS
@db_name	VARCHAR(255), -- Name of finance database
@syscat		VARCHAR(255) = 'No_DB',-- Name of syscat database
@debug		INT = 0
AS
BEGIN
	-- Set variables to be used
	DECLARE @printout	VARCHAR(255),	-- Used to print results and compare values
			@printout2	NVARCHAR(255),	-- Used to print additional results and compare values
			@seedval	INT,			-- Used to check if values are equal
			@sqlstring	NVARCHAR(MAX),	-- Used for executing ALTER statements that use a variable
			@log		VARCHAR(50),	-- Used to hold the log file name
			@ParmDefinition NVARCHAR(500), -- Used for getting count from syscat database
			@count		INT				-- Used for getting count output from syscat	
	
	-- Error checks
	IF @end_v = 0
		RAISERROR('Base version is invalid.',14,1)
	ELSE IF @base_v > @end_v
		RAISERROR('Base version cannot be higher than end version, please check version values.',15,1)
	ELSE IF @end_v != (SELECT (CASE when @end_v > 10.23 then
									CAST(SUBSTRING(au_qtip,0,CHARINDEX('.',au_qtip)) + '.' + REPLACE(SUBSTRING(au_qtip,CHARINDEX('.',au_qtip) + 1,CHARINDEX('.',au_qtip,CHARINDEX('.',au_qtip)) - 1),'.','')AS VARCHAR(10))
								ELSE
									SUBSTRING(cast(au_qtip as varchar(10)),1,1) + substring(cast(@end_v as varchar(10)),CHARINDEX('.',cast(@end_v as varchar(10)),CHARINDEX('.',cast(@end_v as varchar(10)))),2)
								END)
						FROM au_audit_mstr)
		RAISERROR('End version does not match current value in au_audit_mstr.',16,1)
	ELSE IF @db_name IS NULL OR @db_name = '' OR @db_name NOT IN (select name from sys.databases)
		RAISERROR ('Database name is invalid, please use the name of the existing finance database.',17,1)
	ELSE IF @syscat != 'No_DB' AND @syscat NOT IN (select name from sys.databases)
		RAISERROR ('Syscat Database name is invalid, please use the name of the existing syscat database.',18,1)
	ELSE
	BEGIN
		-- Prints out the command issued to run the PAR stored procedure
		SET @printout = @base_v
		SET @printout2 = @end_v
		PRINT 'The executed statement for this result set was:'
		PRINT 'EXEC dbo.sp_par ' + @printout + ',' + @printout2 + ','+ '''' + @db_name + ''''
		PRINT ''
		PRINT ''
		
		-- Defaults check
		PRINT 'Defaults Check:'
		PRINT '---------------'
		SET @printout = (SELECT COUNT(*) FROM ifas_data WHERE category = 'DEFAULTS')
		if @printout = 0
			BEGIN
				PRINT 'ERROR!! # of Defaults = 0'
				PRINT 'Need to run map2vb -b'
			END
		ELSE
			PRINT '# of records where category = DEFAULTS: ' + @printout
		
		-- For versions that are before 7.9 but not a new install
		IF (@base_v < 7.9 and @base_v > 0) OR (@end_v < 7.9)
		BEGIN
			PRINT ''
			PRINT 'GL Rollover:'
			PRINT '------------'
			
			SET @printout = (SELECT COUNT(*) FROM glba_budact_mstr WHERE update_who = 'GL_ROLLOVER') 
				PRINT '# of records in glba_budact_mstr: ' + @printout
			SET @printout = (SELECT COUNT(*) FROM gls_summ_mstr)
				PRINT '# of records in gls_summ_master: ' + @printout
			SET @printout = (SELECT COUNT(*) FROM glc_budg_dtl)
				PRINT '# of records in glc_budg_dtl: ' + @printout
			SET @printout = (SELECT COUNT(*) FROM glm_mo_mstr)
				PRINT '# of records in glm_mo_mstr: ' + @printout
			SET @printout = (SELECT COUNT(*) FROM glba_budact_mstr)
				PRINT '# of records in glba_budact_mstr: ' + @printout
			
			PRINT ''
			PRINT 'OH Rollover:'
			PRINT '------------'
			
			-- This will check to if the values in oh_dtl are the same, if not then additional steps will be taken.
			IF (SELECT COUNT(oh_pe_id) FROM oh_dtl) != (SELECT COUNT(oh_alt_pe_id) FROM oh_dtl)
				BEGIN
					PRINT 'oh_pe_id and oh_alt_pe_id are not the same value, attempting to update...'
					UPDATE oh_dtl SET oh_alt_pe_id = oh_pe_id
					SET @printout = (SELECT COUNT(oh_pe_id) FROM oh_dtl)
					PRINT 'oh_pe_id and oh_alt_pe_id have been set to the value of: ' + @printout
				END
			ELSE	
				PRINT 'No changes needed to be made to oh_pe_id and oh_alt_pe_id values'
				
			SET @printout = (SELECT COUNT(oh_pe_id) FROM oh_dtl)
				PRINT '# of records in oh_dtl.oh_pe_id: ' + @printout
			SET @printout = (SELECT COUNT(oh_alt_pe_id) FROM oh_dtl)
				PRINT '# of records in oh_dtl.oh_alt_pe_id: ' + @printout
			SET @printout = (SELECT COUNT(*) FROM pe_name_mstr WHERE update_who = 'PE_ROLLOVER')
				PRINT '# of records in pe_name_mstr: ' + @printout			
			SET @printout = (SELECT COUNT(pe_id) FROM pe_vendor_dtl)           
				PRINT '# of records in pe_vendor_dtl: ' + @printout
			
			PRINT '' -- Line break for results readability.
			PRINT 'HR Rollover:'
			PRINT '------------'
			
			SET @printout = (SELECT COUNT(*) FROM hr_cntycred_mstr)
			SET @printout2 = (SELECT COUNT(*) FROM hr_cntycred)
				PRINT 'The master table hr_cntycred_mstr holds ' + @printout + ' records and the child table hr_cntycred holds ' + @printout2 + ' records.'
			SET @printout = (SELECT COUNT(*) FROM hr_dfltstcd_mstr)
			SET @printout2 = (SELECT COUNT(*) FROM hr_dfltstcd)
				PRINT 'The master table hr_dfltstcd_mstr holds ' + @printout + ' records and the child table hr_dfltstcd holds ' + @printout2 + ' records.'
			SET @printout = (SELECT COUNT(*) FROM hr_emppaylk_mstr)
			SET @printout2 = (SELECT COUNT(*) FROM hr_emppaylk)
				PRINT 'The master table hr_emppaylk_mstr holds ' + @printout + ' records and the child table hr_emppaylk holds ' + @printout2 + ' records.'
			SET @printout = (SELECT COUNT(*) FROM hr_mstrschd_mstr)
			SET @printout2 = (SELECT COUNT(*) FROM hr_mstrschd)
				PRINT 'The master table hr_mstrschd_mstr holds ' + @printout + ' records and the child table hr_mstrschd holds ' + @printout2 + ' records.'
			SET @printout = (SELECT COUNT(*) FROM hr_nclb_map_mstr)
			SET @printout2 = (SELECT COUNT(*) FROM hr_nclb_map)
				PRINT 'The master table hr_nclb_map_mstr holds ' + @printout + ' records and the child table hr_nclb_map holds ' + @printout2 + ' records.'
			SET @printout = (SELECT COUNT(*) FROM hr_nclbreq_mstr)
			SET @printout2 = (SELECT COUNT(*) FROM hr_nclbreq)
				PRINT 'The master table hr_nclbreq_mstr holds ' + @printout + ' records and the child table hr_nclbreq holds ' + @printout2 + ' records.'
			SET @printout = (SELECT COUNT(*) FROM hr_perdtble_mstr)
			SET @printout2 = (SELECT COUNT(*) FROM hr_perdtble)
				PRINT 'The master table hr_perdtble_mstr holds ' + @printout + ' records and the child table hr_perdtble holds ' + @printout2 + ' records.'
			SET @printout = (SELECT COUNT(*) FROM hr_poscred_mstr)
			SET @printout2 = (SELECT COUNT(*) FROM hr_poscred)
				PRINT 'The master table hr_poscred_mstr holds ' + @printout + ' records and the child table hr_poscred holds ' + @printout2 + ' records.'
			SET @printout = (SELECT COUNT(*) FROM hr_posdflt_mstr)
			SET @printout2 = (SELECT COUNT(*) FROM hr_posdflt)
				PRINT 'The master table hr_posdflt_mstr holds ' + @printout + ' records and the child table hr_posdflt holds ' + @printout2 + ' records.'
			SET @printout = (SELECT COUNT(*) FROM hr_slrytble_mstr)
			SET @printout2 = (SELECT COUNT(*) FROM hr_slrytble)
				PRINT 'The master table hr_slrytble_mstr holds ' + @printout + ' records and the child table hr_slrytble holds ' + @printout2 + ' records.'
			PRINT '' -- Line break for results readability.
		END
		
		PRINT '' -- Line break for results readability.
		PRINT 'Seed Value Check:'
		PRINT '-----------------'
		
		IF @syscat = 'No_DB'
			BEGIN	
				-- This will compare values in bsi_seeds to the current max value.
				-- If something is wrong, additional steps will be taken to correct the issue.
				IF (SELECT seedvalue FROM bsi_seeds WHERE category = 'image_doc_id') < (SELECT MAX(doc_ID) FROM im_info)
					BEGIN
						SET @printout = (SELECT seedvalue FROM bsi_seeds WHERE category = 'image_doc_id')
						SET @printout2 = (SELECT MAX(doc_ID) FROM im_info)
						PRINT 'The seedvalue for image_doc_id was set to: ' + @printout
						PRINT 'The query for MAX(doc_ID) results to: ' + @printout2
						
						PRINT 'The seedvalue for image_doc_id is less than max(doc_id), attempting to update...'
						SET @seedval = (SELECT MAX(doc_ID) FROM im_info)
						UPDATE bsi_seeds SET seedvalue = @seedval WHERE category = 'image_doc_id'
						SET @printout = (SELECT seedvalue FROM bsi_seeds WHERE category = 'image_doc_id')
						PRINT 'Seedvalue for image_doc_id has been updated to: ' + @printout
					END
				ELSE
					BEGIN
						PRINT 'No changes needed to be made to seedvalues.'
						
						SET @printout = (SELECT seedvalue FROM bsi_seeds WHERE category = 'image_doc_id')
						SET @printout2 = (SELECT MAX(doc_ID) FROM im_info)
						PRINT 'The seedvalue for image_doc_id is set to: ' + @printout
						PRINT 'The query for MAX(doc_ID) results to: ' + @printout2
						PRINT ''
					END				
			END
		ELSE IF @syscat != 'No_DB'
			BEGIN
				
				SET @ParmDefinition = N'@countOUT int OUTPUT';
				SET @sqlstring = 'SELECT @countOUT = seedvalue FROM  ' + @syscat + '.dbo.bsi_seeds WHERE category = ''image_doc_id'''
				EXEC sp_executesql @sqlstring,@ParmDefinition,@countOUT=@count OUTPUT
				
				-- This will compare values in bsi_seeds to the current max value.
				-- If something is wrong, additional steps will be taken to correct the issue.
				IF @count < (SELECT MAX(doc_ID) FROM im_info)
					BEGIN
						PRINT 'The seedvalue for image_doc_id is less than max(doc_id), attempting to update...'
						SET @seedval = (SELECT MAX(doc_ID) FROM im_info)
						SET @sqlstring = 'UPDATE ' + @syscat +'.dbo.bsi_seeds SET seedvalue = (SELECT MAX(doc_ID) FROM im_info) WHERE category = ''image_doc_id'''
						EXEC sp_executesql @sqlstring
						PRINT 'Seedvalue for ' + @syscat + '.dbo.image_doc_id has been updated.'
					END
				ELSE
					PRINT 'No changes needed to be made to seedvalues.'
				
				SET @sqlstring = 'SELECT @countOUT = seedvalue FROM  ' + @syscat + '.dbo.bsi_seeds WHERE category = ''image_doc_id'''
				EXEC sp_executesql @sqlstring,@ParmDefinition,@countOUT=@count OUTPUT
				
				SET @printout = @count
				SET @printout2 = (SELECT MAX(doc_ID) FROM im_info)
				PRINT 'The seedvalue for ' + @syscat + '.dbo.image_doc_id is set to: ' + @printout
				PRINT 'The query for MAX(doc_ID) results to: ' + @printout2
				PRINT ''
			END
		
		IF @syscat = 'No_DB'
			BEGIN
				-- This will compare values in bsi_seeds to the current max value.
				-- If something is wrong, additional steps will be taken to correct the issue.
				IF (SELECT seedvalue FROM bsi_seeds WHERE category = 'im_index_id') < (SELECT MAX(index_id) FROM im_index_mstr)
					BEGIN
						SET @printout = (SELECT seedvalue FROM bsi_seeds WHERE category = 'im_index_id')
						SET @printout2 = (SELECT MAX(index_id) FROM im_index_mstr)
						PRINT 'The seedvalue for im_index_id is set to: ' + @printout
						PRINT 'The query for MAX(index_id)) results to: ' + @printout2
						
						PRINT 'The seedvalue for im_index_id is less than max(index_id), attempting to update...'
						SET @seedval = (SELECT MAX(index_id) FROM im_index_mstr)
						UPDATE bsi_seeds SET seedvalue = @seedval WHERE category = 'im_index_id'
						SET @printout = (SELECT seedvalue FROM bsi_seeds WHERE category = 'im_index_id')
						PRINT 'Seedvalue for im_index_id has been updated to: ' + @printout
					END
				ELSE
					BEGIN
						PRINT 'No changes needed to be made to seedvalues.'
						
						SET @printout = (SELECT seedvalue FROM bsi_seeds WHERE category = 'im_index_id')
						SET @printout2 = (SELECT MAX(index_id) FROM im_index_mstr)
						PRINT 'The seedvalue for im_index_id is set to: ' + @printout
						PRINT 'The query for MAX(index_id)) results to: ' + @printout2
						PRINT ''
					END

			END
		ELSE IF @syscat != 'No_DB'
			BEGIN
				SET @ParmDefinition = N'@countOUT int OUTPUT';
				SET @sqlstring = 'SELECT @countOUT = seedvalue FROM  ' + @syscat + '.dbo.bsi_seeds WHERE category = ''im_index_id'''
				EXEC sp_executesql @sqlstring,@ParmDefinition,@countOUT=@count OUTPUT
				
				-- This will compare values in bsi_seeds to the current max value.
				-- If something is wrong, additional steps will be taken to correct the issue.
				IF @count < (SELECT MAX(index_id) FROM im_index_mstr)
					BEGIN
						PRINT 'The seedvalue for im_index_id is less than max(index_id), attempting to update...'
						SET @seedval = (SELECT MAX(index_id) FROM im_index_mstr)
						SET @sqlstring = 'UPDATE ' + @syscat + '.dbo.bsi_seeds SET seedvalue = (SELECT MAX(index_id) FROM im_index_mstr) WHERE category = ''im_index_id'''
						EXEC sp_executesql @sqlstring
						PRINT 'Seedvalue for ' + @syscat + '.dbo.im_index_id has been updated.'
					END
				ELSE
					PRINT 'No changes needed to be made to seedvalues.'
				
				SET @sqlstring = 'SELECT @countOUT = seedvalue FROM  ' + @syscat + '.dbo.bsi_seeds WHERE category = ''im_index_id'''
				EXEC sp_executesql @sqlstring,@ParmDefinition,@countOUT=@count OUTPUT
				
				SET @printout = @count
				SET @printout2 = (SELECT MAX(index_id) FROM im_index_mstr)
				PRINT 'The seedvalue for ' + @syscat + '.dbo.im_index_id is set to: ' + @printout
				PRINT 'The query for MAX(index_id) results to: ' + @printout2
				PRINT ''
			END
		
		-- This will compare values in ds_subs_mstr to the current max value of glt_job_no in glt_trns_dtl.
		-- If something is wrong, additional steps will be taken to correct the issue.
		IF (SELECT ds_seed_vect01 FROM ds_subs_mstr WHERE ds_sub = 'NU') < (SELECT MAX(glt_job_no) FROM glt_trns_dtl)
			BEGIN
				-- This check will determine if an update will be done on the ds_subs_mstr table to re-set the job number
				-- If the updated value is too large a warning will be issued with the statements used for the update statement
				IF (((SELECT MAX(glt_job_no) FROM glt_trns_dtl) - (SELECT ds_seed_vect01 FROM ds_subs_mstr WHERE ds_sub = 'NU')) > 1000)
					BEGIN
						PRINT ''
						PRINT '*******************************************************'
						PRINT '********** THERE IS AN ISSUE WITH THE *****************'
						PRINT '********** UPDATE STATEMENT PLEASE READ!!! ************'
						PRINT '*******************************************************'
						PRINT ''
						
						PRINT 'The following update statement will increase the job number substantially.'
						PRINT 'The statement will increase the value from ' + @printout2 + ' to ' + @printout
						PRINT 'Please verify if these statements are correct.'
						SET @sqlstring = (SELECT cast(MAX(glt_job_no) as varchar(100)) FROM glt_trns_dtl)
						PRINT ''
						PRINT 'SELECT MAX(glt_job_no) FROM glt_trns_dtl'
						PRINT 'SELECT ds_seed_vect01 FROM ds_subs_mstr WHERE ds_sub = ''NU'''
						PRINT '--UPDATE ds_subs_mstr SET ds_seed_vect01 = ' + @sqlstring + ' WHERE ds_sub = ''NU'''
						PRINT ''
						PRINT '*******************************************************'
						PRINT '*******************************************************'
					END
				ELSE
					BEGIN
						SET @printout = (SELECT MAX(glt_job_no) FROM glt_trns_dtl)
						SET @printout2 = (SELECT ds_seed_vect01 FROM ds_subs_mstr WHERE ds_sub = 'NU')
						PRINT 'The value for max(glt_job_no) is set to: ' + @printout
						PRINT 'The value for ds_seed_vect01 is set to: ' + @printout2
				
						PRINT 'The seedvalue for ds_seed_vect01 is less than MAX(glt_job_no), attempting to update...'
						SET @seedval = (SELECT MAX(glt_job_no) FROM glt_trns_dtl)
						UPDATE ds_subs_mstr SET ds_seed_vect01 = @seedval WHERE ds_sub = 'NU'
						SET @printout2 = (SELECT ds_seed_vect01 FROM ds_subs_mstr WHERE ds_sub = 'NU')
						PRINT 'Value for ds_seed_vect01 has been updated to: ' + @printout2
					END				
			END
		ELSE
			BEGIN
				PRINT 'No changes needed to be made for ds_seed_vect01.'
				SET @printout = (SELECT MAX(glt_job_no) FROM glt_trns_dtl)
				SET @printout2 = (SELECT ds_seed_vect01 FROM ds_subs_mstr WHERE ds_sub = 'NU')
				PRINT 'The value for max(glt_job_no) is set to: ' + @printout
				PRINT 'The value for ds_seed_vect01 is set to: ' + @printout2
				PRINT ''
			END
			
		
		
		PRINT '' -- Line break for results readability.
		PRINT 'Database Settings Check:'
		PRINT '------------------------'
		
		SET @sqlstring = NULL
		SET @sqlstring = (SELECT cd_descl FROM cd_codes_mstr WHERE cd_category = 'CKCV' AND cd_code = 'CKID')
		IF @sqlstring IS NULL
			BEGIN
				PRINT 'oh090mop needs to be run'
				PRINT 'Open ksh and type in: run oh090mop'
				PRINT 'Run this select statement to verify it completed successfully'
				PRINT 'select cd_descl from cd_codes_mstr where cd_category = ''CKCV'' and cd_code = ''CKID'''
				PRINT ''
			END
		ELSE
			BEGIN
				PRINT 'oh090mop has already been ran'
				PRINT 'No further actions are needed'
				PRINT ''
			END
		
		SET @printout = NULL
		-- Needs to be executed for all versions
		SET @printout = (SELECT is_read_committed_snapshot_on FROM sys.databases WHERE name = @db_name)
		IF @printout = '0' -- 0 means committed snapshot is off and additional steps will be taken
			BEGIN
				-- This will use the database name variable to create a statement to turn on committed snapshot.
				-- Once executed, the results will print out that it either has been fixed or an error has occurred.
				PRINT 'Read committed snapshot is set to "OFF", attempting to fix...'
				SET @sqlstring = 'ALTER DATABASE ' + @db_name + ' SET READ_COMMITTED_SNAPSHOT ON with rollback immediate'
				EXEC sp_executesql @sqlstring
				SET @printout = (SELECT is_read_committed_snapshot_on FROM sys.databases WHERE name = @db_name)
				IF @printout = '1'
					PRINT 'Read committed snapshot is now set to "ON"'
				ELSE
					PRINT 'Error turning committed snapshot on, stop any/all connected services to ' + @db_name + ' and run: ALTER DATABASE ' + @db_name + ' SET READ_COMMITTED_SNAPSHOT ON with rollback immediate'
			END
		ELSE
			PRINT 'Read committed snapshot is set to "ON"'
		
		-- Check to see if compatablilty level is set for sql server 2000 (level 80).  This needs to be checked for all installs.
		-- If not then additional steps will be taken to correct the issue.
		SET @printout = (SELECT compatibility_level FROM sys.databases WHERE name = @db_name)
		IF @end_v < 11.12 and @printout != 80
			BEGIN
				PRINT 'Compatability level was set incorrectly, attempting to fix...'
				SET @sqlstring = 'ALTER DATABASE ' + @db_name + ' SET COMPATIBILITY_LEVEL = 80'
				EXEC sp_executesql @sqlstring
				SET @printout = (SELECT compatibility_level FROM sys.databases WHERE name = @db_name)
				PRINT 'Compatability level is now set to: ' + @printout
			END
		ELSE
			PRINT 'Compatability level is set to: ' + @printout
		
		-- Check to see if the recovery model is set correctly: SIMPLE.
		-- If not then additional steps will be taken to correct the issue.
		SET @printout = (SELECT recovery_model_desc FROM sys.databases WHERE name = @db_name)
		IF @printout != 'SIMPLE'
			BEGIN
				PRINT 'Recovery model was set incorrectly, attempting to fix...'
				SET @sqlstring = 'ALTER DATABASE ' + @db_name + ' SET RECOVERY SIMPLE'
				EXEC sp_executesql @sqlstring
				SET @printout = (SELECT recovery_model_desc FROM sys.databases WHERE name = @db_name)
				PRINT 'Recovery model now set to: ' + @printout
			END
		ELSE
			PRINT 'Recovery model is set to: ' + @printout
		
		IF NOT EXISTS (SELECT 1 FROM master.dbo.sysprocesses WHERE program_name = N'SQLAgent - Generic Refresher') 
			BEGIN
				PRINT N'The SQL Server Agent process is not running.'
			END
		ELSE 
			BEGIN
				PRINT N'The SQL Server Agent process is running.'
			END
		
		PRINT ''

		SET @sqlstring = NULL		-- Reset executable sql string to NULL
		DECLARE log_cur CURSOR FOR	-- Creating cursor for shrinking log files.
			(SELECT name FROM sys.database_files where type_desc = 'LOG')
		
		-- Opens cursor addcon_cur and selects the first result into a varable if there is one.
		OPEN log_cur
		FETCH NEXT FROM log_cur INTO @log
		
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SET @sqlstring = 'DBCC SHRINKFILE (' + @log + ', 1) WITH NO_INFOMSGS'
					EXEC sp_executesql @sqlstring	-- Executes statement to add default constraint.
					print 'SQL statement was ran to shrink Log File ' + @log + '.ldf to 1MB for the database: ' + @db_name
					
					FETCH NEXT FROM log_cur INTO @log	-- Selects the next record to execute, if none, then exit.
				END -- end while loop
		
			CLOSE log_cur	-- Close the cursor
		DEALLOCATE log_cur	-- Deallocates cursor memory stored

	END
END
GO

--------------------
-- End sp_par.sql --
--------------------