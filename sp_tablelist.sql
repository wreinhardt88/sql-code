if object_id('sp_tablelist') is not null
	begin
		drop procedure dbo.sp_tablelist
	end
go

create procedure dbo.sp_tablelist
@table varchar(255) = null
as
begin
	if @table is null
	begin
		if object_id('sp_tablelist') is not null
			begin
				drop table dbo.mig_table_list
			end
		create table dbo.mig_table_list(tablename varchar(255) null)
	end
	else if @table is not null
	begin
		insert into dbo.mig_table_list values(@table)
	end
end